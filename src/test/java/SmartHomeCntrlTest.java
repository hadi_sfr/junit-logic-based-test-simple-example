import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SmartHomeCntrlTest {

    @Test
    public void testFireInNormalDayNoFireSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 2, 2),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Open, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationDayNoFireSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 2),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInNormalNightNoFireSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 2, 2),
                Time.Night
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInNormalDayNoFireNoSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 2),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInNormalDayNoFireSmokeNoGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 2, 0),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInNormalDayNoFireNoSmokeNoGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 0),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInThiefDayFireSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(0, 2, 2),
                Time.Day
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testFireInVacationDayFireSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 2),
                Time.Day
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInThiefDayNoFireSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(0, 2, 2),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInThiefDayFireNoSmokeNoGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(0, 0, 0),
                Time.Day
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInThiefDayFireNoSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(0, 0, 2),
                Time.Day
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testFireInThiefDayFireSmokeNoGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(0, 2, 0),
                Time.Day
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testFireInVacationNightFireSmokeGasOpenDoor() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 2),
                Time.Night
        );
        smartHomeController.getHomeState().setDoors(Doors.Open);
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Open, Lights.Emergency, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationDayFireSmokeGasOpenDoor() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 2),
                Time.Day
        );
        smartHomeController.fire(2);
        smartHomeController.getHomeState().setDoors(Doors.Open);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Open, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationNightFireSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 2),
                Time.Night
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationNightNoFireSmokeGasOpenDoor() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 2),
                Time.Night
        );
        smartHomeController.getHomeState().setDoors(Doors.Open);
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Open, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationNightFireNoSmokeGasOpenDoor() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 0, 2),
                Time.Night
        );
        smartHomeController.getHomeState().setDoors(Doors.Open);
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Open, Lights.Emergency, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationNightFireSmokeNoGasOpenDoor() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 0),
                Time.Night
        );
        smartHomeController.getHomeState().setDoors(Doors.Open);
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Open, Lights.Emergency, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationNightFireNoSmokeNoGasOpenDoor() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 0, 0),
                Time.Night
        );
        smartHomeController.getHomeState().setDoors(Doors.Open);
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Open, Lights.Emergency, Alarm.Off)
        ));
    }

    @Test
    public void testSmokeInNormalDayLowEmerg() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 0),
                Time.Day
        );
        smartHomeController.smoke(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testSmokeInNormalNightLowEmerg() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 0),
                Time.Night
        );
        smartHomeController.getHomeState().setWindows(Windows.Open);
        smartHomeController.getHomeState().setDoors(Doors.Open);
        smartHomeController.smoke(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testSmokeInNormalDayHighEmerg() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(2, 2, 2),
                Time.Day
        );
        smartHomeController.smoke(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.Off)
        ));
    }

    @Test
    public void testGasInVacationNight() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(1, 1, 0),
                Time.Night
        );
        smartHomeController.gas(1);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.Off)
        ));
    }

    @Test
    public void testGasInVacationDay() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(1, 1, 0),
                Time.Day
        );
        smartHomeController.gas(1);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testGasInThiefDayLowEmerg() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(1, 1, 0),
                Time.Day
        );
        smartHomeController.gas(1);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testGasInThiefNightHighEmergHighSense() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Thief_Mode),
                new EnvState(2, 2, 0),
                Time.Night
        );
        smartHomeController.gas(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testGasInNormalDayLowEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 0),
                Time.Day
        );
        smartHomeController.gas(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Off, Alarm.On)
        ));
    }

    @Test
    public void testGasInNormalNightHighEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Normal_Mode),
                new EnvState(2, 2, 0),
                Time.Night
        );
        smartHomeController.gas(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Open, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testGasInNormalDayLowEmergLowSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 0),
                Time.Day
        );
        smartHomeController.gas(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testGasInNormalNightLowEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 0),
                Time.Night
        );
        smartHomeController.gas(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Off, Alarm.On)
        ));
    }

    @Test
    public void testGasInNormalDayHighEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Normal_Mode),
                new EnvState(2, 2, 0),
                Time.Day
        );
        smartHomeController.gas(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Off, Alarm.On)
        ));
    }

    @Test
    public void testGasInNormalNightHighEmergLowSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(2, 2, 0),
                Time.Night
        );
        smartHomeController.gas(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testGasInThiefDayHighEmerg() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(2, 2, 0),
                Time.Day
        );
        smartHomeController.gas(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testFireInNormalNightNoFireNoSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 0, 2),
                Time.Night
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInNormalNightNoFireSmokeNoGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Normal_Mode),
                new EnvState(0, 2, 0),
                Time.Night
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInThiefDayNoFireNoSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(0, 0, 2),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInThiefDayNoFireSmokeNoGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(0, 2, 0),
                Time.Day
        );
        smartHomeController.fire(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationDayFireSmokeNoGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 2, 0),
                Time.Day
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testFireInVacationDayFireNoSmokeGas() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Vacation_Mode),
                new EnvState(0, 0, 2),
                Time.Day
        );
        smartHomeController.fire(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testSmokeInThiefDayHighEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Thief_Mode),
                new EnvState(2, 0, 2),
                Time.Day
        );
        smartHomeController.smoke(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testSmokeInThiefNightLowEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Thief_Mode),
                new EnvState(0, 0, 0),
                Time.Night
        );
        smartHomeController.smoke(0);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testSmokeInThiefNightHighEmergLowSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(1, SecurityMode.Thief_Mode),
                new EnvState(2, 0, 2),
                Time.Night
        );
        smartHomeController.smoke(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Close, Doors.Close, Lights.Off, Alarm.Off)
        ));
    }

    @Test
    public void testSmokeInVacationNightHighEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Vacation_Mode),
                new EnvState(2, 0, 2),
                Time.Night
        );
        smartHomeController.smoke(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Close, Lights.Emergency, Alarm.On)
        ));
    }

    @Test
    public void testSmokeInThiefNightHighEmergHighSens() {
        SmartHomeController smartHomeController = new SmartHomeController(
                new SecurityConfig(5, SecurityMode.Thief_Mode),
                new EnvState(2, 0, 2),
                Time.Night
        );
        smartHomeController.smoke(2);
        assertTrue(smartHomeController.getHomeState().equals(
                new HomeState(Windows.Open, Doors.Open, Lights.Emergency, Alarm.Off)
        ));
    }

}
